require 'rake/testtask'
require 'json'
require 'httparty'

MOBILE_DETECT_JSON = 'https://raw.github.com/serbanghita/Mobile-Detect/master/Mobile_Detect.json'
TABLET_FILE        = 'lib/mobile-fu/tablet.rb'

task :default => [:test]

# Rails backtrace cleaner breaks minitest warning code added in 5.12.0, because
# it doesn't check for the case where backtrace cleaning completely leaves an
# empty backtrace. Force no backtrace cleaning to workaround that.
ENV["BACKTRACE"] = "true"

Rake::TestTask.new do |t|
  t.libs.push 'lib'
  t.test_files = FileList['spec/*_spec.rb', 'spec/mobile-fu/*_spec.rb']
  t.verbose = true
end
